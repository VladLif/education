<?php

// Функция возрата списка всех пользователей
function getUsersList(): array
{
 return [
     'admin' => '$2y$10$SQYjQ3rrZXQleQmelh54kuArWX2Ht6WFHPLnWQR0X1JFD8SZQJMuu',
     'admin1' => '$2y$10$.AMgBH3OUowHPAe29qHWcODSjV7PCdyWO.Pdlt/Kv/RWJsbBvYRU6',
     'admin2' => '$2y$10$ApKNoqLpYiYfm8RyUTmlPOpQsLX.T2zjlfCyLRtK9FgYtudux9K.K',
 ];
}

//Функция проверки логина пользователя
function existsUser($login)
{
    foreach (array_keys(getUsersList()) as $name){
        if($name == $login){
            return true;
        }
    }
    return false;
}

//Функция проверки пароля
function checkPassword($login, $password)
{
    if(existsUser($login) && password_verify($password, getUsersList()[$login])){
        return true;
    }
    return false;
}

//Функция приветствия пользователя
function getCurrentUser()
{
    return $_SESSION['username'];
}

if(isset($_POST['name']) && isset($_POST['pass'])) {
    if (checkPassword($_POST['name'], $_POST['pass'])) {
        session_start();
        $_SESSION['username'] = $_POST['name'];
        header('Location: hw_4_image.php');
    }else{
        $_SESSION['username'] = null;
        header('Location: login.php');
    }
}
?>