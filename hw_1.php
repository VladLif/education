<?php
var_dump(3 / 1);
var_dump(1 / 3);
var_dump('20cats' + 40);
var_dump(18 % 4);

echo ($a = 2); // Значением выражения присваивания будет выражение стоящее после знака '='
$x = ($y = 12) - 8;
echo ($x . "\n");

var_dump(1 == 1.0); //Будет истина, потому что 2 знака равно сравнивает только значения
var_dump(1 === 1.0); //Будет ложь, потому что 3 знака равно сравнивает и значения и тип данных
var_dump('02' == 2); //Будет истина, потому что 2 знака равно сравнивает только значения
var_dump('02' === 2); //Будет ложь, потому что 3 знака равно сравнивает и значения и тип данных
var_dump('02' == '2'); //Будет истина, потому что 2 знака равно сравнивает только значения
$x = true xor true;
var_dump($x); /*Истина, потому что в $x присваивается значение после знака равно, а результат с исключающим или
не записывается не в какую переменную */
?>