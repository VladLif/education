<?php
//Таблица истинности
//Функция конъюнкция
function output_and($a, $b)
{
    if($a && $b){
        return 1;
    } else {
        return 0;
    }
}

//Функция дизъюнкция
function output_or($a, $b)
{
    if($a || $b){
        return 1;
    } else {
        return 0;
    }
}
//Функция исключающее или
function output_xor($a, $b)
{
    if(($a==0 && $b==0)||($a!=0 && $b!=0)){
        return 0;
    } else {
        return 1;
    }
}

//Функция для решения квадратного уравнения
function calculating_the_discriminant($a, $b, $c)
{
    $d = $b ** 2 - 4 * $a * $c;
    if($d > 0){
        $x_1 = (- $b + $d ** (1/2))/(2 * $a);
        $x_2 = (- $b - $d ** (1/2))/(2 * $a);
        return $x_1 .", ". $x_2;
    } elseif($d==0){
        $x_1 = (- $b / (2 * $a));
        return $x_1;
    } else{
        return "Корней нет";
    }
}

echo "Конъюнкция<br/>";
echo "a = 0, b = 0: " . output_and(0, 0) . "<br/>";
echo "a = 1, b = 0: " . output_and(1, 0) . "<br/>";
echo "a = 0, b = 1: " . output_and(0, 1) . "<br/>";
echo "a = 1, b = 1: " . output_and(1, 1) . "<br/>";

echo "<br/>Дизъюнкция<br/>";
echo "a = 0, b = 0: " . output_or(0, 0) . "<br/>";
echo "a = 1, b = 0: " . output_or(1, 0) . "<br/>";
echo "a = 0, b = 1: " . output_or(0, 1) . "<br/>";
echo "a = 1, b = 1: " . output_or(1, 1) . "<br/>";

echo "<br/>Исключающее или<br/>";
echo "a = 0, b = 0: " . output_xor(0, 0) . "<br/>";
echo "a = 1, b = 0: " . output_xor(1, 0) . "<br/>";
echo "a = 0, b = 1: " . output_xor(0, 1) . "<br/>";
echo "a = 1, b = 1: " . output_xor(1, 1) . "<br/>";

echo "<br/>Корень/корни уравнения:\n" . calculating_the_discriminant(1, -5, 9);
//Тесты
assert(1 == calculating_the_discriminant(1, 17, -18));
assert("Корней нет" == calculating_the_discriminant(1, -5, 9));

?>